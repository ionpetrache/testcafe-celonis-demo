FROM selenium/standalone-chrome

# Install Node JS
RUN curl -sL http://nsolid-deb.nodesource.com/nsolid_setup_3.x | sudo bash -
RUN sudo apt-get -y install nsolid-carbon nsolid-console

CMD [ "node" ]