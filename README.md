# testcafe-celonis-demo

## General requirments
Mac OS X, Linux with a newer version of node and git.
On my side it was tested on **Mac OS X** with
`node -v && npm -v
v8.11.3
5.6.0`

It should be easy to make it run on Linux as both are UNIX systems. On Windows it could be that some path issues might be encourtered.

## Browsers
* You need at least one browser from the list Chrome, Firefox, Opera, Safari
* There is also the posibility to run in headless mode in case you want to run the tests only in CI.

## Project structure
It is a simple [TestCafe](https://devexpress.github.io/testcafe/) based project. The actual test scenarios are [spec files](./tests/health.spec.js). High level steps functions have been created to avoid duplication in test functions and those steps can be found in [steps](./tests/steps) folder. Also in order to isolate the location of elements, page objects with locators have been created in [po](./tests/po) folder.

## How to run the tests on local machine
1. Open the terminal and clone the repo: `git clone https://gitlab.com/ionpetrache/testcafe-celonis-demo.git
`
2. Change to the root folder of the project
3. Install all the dependencies: `npm install`

4. There are a couple of scripts created in [package.json](./package.json) file. So you have the following posibilities:
* Chrome with UI: `npm run test:chrome`
* Chrome headless: `npm run test:chrome:headless`
* Firefox with UI: `npm run test:firefox`
* Firefox headless: `npm run test:firefox:headless`
* Safari with UI: `npm run test:safari`
* Opera with UI: `npm run test:opera`
* On your mobile device: `npm run test:mobile`. Then scan the generated QR code that you see in the terminal. Wait a bit for TestCafe to start your test.

5. Test reports are generated inside [reports](./reports) folder.

6. For the failures, screenshots are being generated in [screenshots](./screenshots) folder.

7. The actual test scenarios can be found in [health.spec.js](./tests/health.spec.js)


## GitLab CI configuration
There is a GitLab CI pipeline configured for the tests to run every day at noon. You could also start it manually if you want. In order to check for the results go to the [repository](https://gitlab.com/ionpetrache/testcafe-celonis-demo) -> CI / CD -> Schedules. To check for the previous results of the pipeline go to: CI / CD -> Pipelines. You can also download the generated test reports.

## Docker image
In order to run the tests in CI I've created a custome image with chome browser. This is used as the base for the CI container [Dockerfile](./Dockerfile). The image that was created from the Dockerfile is uploaded in the [repository GitLab registry](https://gitlab.com/ionpetrache/testcafe-celonis-demo/container_registry).

