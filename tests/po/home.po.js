import { Selector } from 'testcafe';

export default class homePage {
    constructor() {
        this.url = 'https://trialcloud.celonis.com/#/frontend/projects';
        this.title = Selector('title');
        this.projectItemName = Selector('div.project-item-name');
        this.breadcrumb = Selector('span.breadcrumb__link');
        this.logo = Selector('div[title="Celonis Home"]');
    }
}
