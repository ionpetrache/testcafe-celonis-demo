import { Selector } from 'testcafe';

export default class LoginPage {
    constructor() {
        this.url = 'https://trialcloud.celonis.com/#/frontend/projects';
        this.title = Selector('title');
        this.inputName = Selector('input[name="email"]');
        this.inputPass = Selector('#password');
        this.buttonLogin = Selector('button.trial__button');
    }
}
