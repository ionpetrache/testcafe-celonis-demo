import LoginPage from './po/login.po';
import HomePage from './po/home.po';
import settings from '../settings';

const loginPage = new LoginPage();
const homePage = new HomePage();

fixture `Health check for the Deamons`
    .page(loginPage.url);

test('Scenario 1: Check every one of the 6 Deamon states', async t => {
    const analisysList = ['1. Purchase to Pay', '2. Accounts Payable',
        '3. Order to Cash', '4. IT Service Management',
        '5. Logistics', '6. Human resources'];

    await t
        .expect(loginPage.title.innerText).eql('Log in - Celonis trial ')
        .typeText(loginPage.inputName, settings.USERNAME)
        .typeText(loginPage.inputPass, settings.PASSWORD)
        .click(loginPage.buttonLogin)
        .expect(homePage.title.innerText).eql('Celonis Process Mining')
        .expect(homePage.breadcrumb.innerText).eql('Projects')
        .expect(homePage.projectItemName.innerText).eql('Example Projects')
        .doubleClick(homePage.projectItemName)
        .expect(homePage.breadcrumb.innerText).eql('Example Projects')
        .doubleClick(homePage.projectItemName.withText(analisysList[0]))
        .expect(homePage.title.innerText).eql(analisysList[0])
        .click(homePage.logo)
        .doubleClick(homePage.projectItemName.withText(analisysList[1]))
        .expect(homePage.title.innerText).eql(analisysList[1])
        .click(homePage.logo)
        .doubleClick(homePage.projectItemName.withText(analisysList[2]))
        .expect(homePage.title.innerText).eql(analisysList[2])
        .click(homePage.logo)
        .doubleClick(homePage.projectItemName.withText(analisysList[3]))
        .expect(homePage.title.innerText).eql(analisysList[3])
        .click(homePage.logo)
        .doubleClick(homePage.projectItemName.withText(analisysList[4]))
        .expect(homePage.title.innerText).eql(analisysList[4])
        .click(homePage.logo)
        .doubleClick(homePage.projectItemName.withText(analisysList[5]))
        .expect(homePage.title.innerText).eql(analisysList[5]);
});

