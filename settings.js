require('dotenv').config();

const settings = {
    USERNAME: process.env.USERNAME,
    PASSWORD: process.env.PASSWORD
};

export default settings;